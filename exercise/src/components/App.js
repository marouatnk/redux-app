import React from 'react';
import { Component } from 'react';
import logo from '../logo.svg';
import '../App.css';
import { connect } from 'react-redux';
import { switchToggle } from '../actions/action';
import { updateInput1 } from '../actions/calc';
import { updateInput2 } from '../actions/calc';
import { updateOperation } from '../actions/calc';
import { updateInput1WithRandom } from '../actions/calc';
import { calcResult } from '../actions/calc';
import { bindActionCreators }  from 'redux';
import Result from './Result';
import InputNumber from './InputNumber';
import Operation from './Operation';
import Button from './Button';

class App extends Component{
  
  render(){
    console.log(this.props);
    return (
        <div className="App">
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
                {this.props.toggle
                    ? 'REACT'
                    : 'REDUX'      }
                <button onClick={this.props.switchToggle}>TOGGLE</button>

                    <InputNumber onChange={ (event) => this.props.updateInput1(event.target.value) } value={this.props.input1}/>
                    <InputNumber onChange={ (event) => this.props.updateInput2(event.target.value) } value={this.props.input2}/>

                    <Operation  onChange={ (event) => this.props.updateOperation(event.target.value) } />

                    <Button label="RAND" onClick={this.props.updateInput1WithRandom}/>
                    <Button label="CALC" onClick={this.props.calcResult} />

                    <Result />
            </header>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      toggle: state.toggleReducer.toggle,
      input1: state.calcReducer.input1,
      input2: state.calcReducer.input2
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ switchToggle , updateInput1 , updateInput2 , updateOperation, updateInput1WithRandom, calcResult }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
