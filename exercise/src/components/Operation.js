import React from 'react';

class Operation extends React.Component{

    render(){
        return (
            <select onChange={this.props.onChange}>
                <option value="+">+</option>
                <option value="-">-</option>
                <option value="*">*</option>
                <option value="/">/</option>
            </select>
        );
    }
}

export default Operation;