import {calc} from '../services/calc';

const INITIAL_STATE = {
    'input1' : 20,
    'input2' : 10,
    'operation' : '+',
    'result' : 30,
    'loader': false
}

const calcReducer = function (state = INITIAL_STATE, action) {
  switch (action.type) {
        case 'UPDATE_INPUT1':
          return {
            ...state,
            input1: action.value
          }
        case 'UPDATE_INPUT2':
          return {
            ...state,
            input2: action.value
        }
        case 'UPDATE_OPERATION':
          return {
            ...state,
              operation: action.value
        }
        case 'UPDATE_RESULT':
          return {
            ...state,
              result: calc(state.input1 , state.input2, state.operation)
        }
        default:
          return state
      }
}

export default calcReducer;