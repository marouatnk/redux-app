import { combineReducers } from 'redux'
import toggleReducer from './toggleReducer';
import calcReducer from './calcReducer';

export default combineReducers({
    toggleReducer,
    calcReducer
})