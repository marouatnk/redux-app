const INITIAL_STATE = {
    toogle : false
};

const toggleReducer = function (state = { INITIAL_STATE }, action) {
    switch (action.type) {
      case 'SWITCH_TOGGLE':
        return { toggle: !state.toggle }
      default:
        return state
    }
  }

  export default toggleReducer;