export const updateInput1 = (value) => {
    return {
        type: 'UPDATE_INPUT1',
        value: parseInt(value)
    }
}
export const updateInput2 = (value) => {
    return {
        type: 'UPDATE_INPUT2',
        value: parseInt(value)
    }
}
export const updateOperation = (value) => {
    return {
        type: 'UPDATE_OPERATION',
        value
    }
}

export const updateInput1WithRandom = (value) => {
    return {
        type: 'UPDATE_INPUT1',
        value : Math.floor(Math.random() * Math.floor(20000))
    }
}

export const calcResult = () => {
    return {
        type: 'UPDATE_RESULT'
    }
}

export const updateLoader = (status) => {
    return {
        type : 'UPDATE_LOADER',
        status
    }
}

export const updateInput1WithFetchedRandom = () => {
    return async (dispatch) => {
        dispatch(updateLoader(true));
        // http://www.randomnumberapi.com/api/v1.0/random?min=100&max=1000&count=1
        const response = await fetch('http://www.randomnumberapi.com/api/v1.0/random?min=100&max=1000&count=1');
        const data = await response.json();
        dispatch(updateInput1(data[0]));
        dispatch(updateLoader(false));
    }
}