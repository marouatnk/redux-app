export const calc = (input1, input2, operation) => {

    let calc = 0;

    if(operation === '+')
        calc = input1 + input2;
    if(operation === '-')
        calc = input1 - input2;
    if(operation === '/')
        calc = input1 / input2;
    if(operation === '*')
        calc = input1 * input2;

    return parseFloat(calc);
}